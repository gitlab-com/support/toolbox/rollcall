# Roll Call

A script to get and show all members in a GitLab.com group, including members in each subgroup and projects. When using a personal access token for an admin account, the script will also return users' [highest access level](https://docs.gitlab.com/ee/api/access_requests.html).

## Usage

```
Usage of rollcall:
  -csv
        CSV output of users.
  -g int
        (required) Group ID of GitLab.com group. (default -1)
  -json
        JSON output of users.
  -t string
        (required) GitLab.com Personal Access Token.
```

Use the `-t` flag to provide a GitLab.com personal access token and the `-g` flag for the GitLab.com group you're running the script for.

You can use either the `-json` or `-csv` flags to output in JSON or CSV, respectively. If you want to save it to a file, use shell redirects, for example: `rollcall -t <token> -g <id> --csv > users.txt`.
