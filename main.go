package main

import (
	"encoding/csv"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

const (
	baseUrl = "https://gitlab.com/api/v4/"
)

// Only need the ids from project objects
type objectId struct {
	ID int `json:"id"`
}

type objectIds []objectId

type user struct {
	ID          int    `json:"id"`
	Username    string `json:"username"`
	Name        string `json:"name"`
	PublicEmail string `json:"public_email"`
	HighestRole int    `json:"highest_role"`
}

var token string
var gid int

var jsonOut = flag.Bool("json", false, "JSON output of users.")
var csvOut = flag.Bool("csv", false, "CSV output of users.")

func init() {
	flag.StringVar(&token, "t", "", "(required) GitLab.com Personal Access Token.")
	flag.IntVar(&gid, "g", -1, "(required) Group ID of GitLab.com group.")
}

func main() {
	flag.Parse()
	if gid <= 0 || len(token) <= 0 {
		fmt.Println("Please provide valid group ID and personal access token.\n")
		fmt.Fprintf(os.Stderr, "usage of %s\n:", os.Args[0])
		flag.PrintDefaults()
		os.Exit(1)
	}
	if *jsonOut && *csvOut {
		fmt.Println("Please only select one output type - JSON, CSV, or normal.")
		os.Exit(1)
	}
	users := make(map[int]user)
	getMembers(gid, users)

	if *jsonOut {
		out, err := jsonOutput(users)
		if err != nil {
			log.Fatalf("%s", err)
		}
		fmt.Printf("%s", out)
		os.Exit(0)
	}

	if *csvOut {
		out := csvOutput(users)
		w := csv.NewWriter(os.Stdout)
		for _, r := range out {
			if err := w.Write(r); err != nil {
				log.Fatalf("error writing to csv: %s", err)
			}
		}
		w.Flush()
		if err := w.Error(); err != nil {
			log.Fatal(err)
		}
		os.Exit(0)
	}

	// Regular Output
	for _, u := range users {
		fmt.Printf("id: %d\nusername: %v\nname: %v\npublic_email: %v\nhighest_role: %d\n", u.ID, u.Username, u.Name, u.PublicEmail, u.HighestRole)
		fmt.Println("")
	}
	fmt.Println("----------------------------------")
	fmt.Printf("Total: %d members\n", len(users))
}

func jsonOutput(users map[int]user) ([]uint8, error) {
	var output []user
	for _, user := range users {
		output = append(output, user)
	}
	j, err := json.MarshalIndent(output, "", "  ")
	if err != nil {
		return nil, fmt.Errorf("error marshalling json: %s", err)
	}

	return j, nil
}

func csvOutput(users map[int]user) [][]string {
	output := [][]string{{"id", "username", "name", "public_email", "highest_role"}}
	for _, user := range users {
		id := fmt.Sprintf("%d", user.ID)
		highest_role := fmt.Sprintf("%d", user.HighestRole)
		u := []string{id, user.Username, user.Name, user.PublicEmail, highest_role}
		output = append(output, u)
	}
	return output
}

func getRequest(endpoint string) (*http.Response, error) {
	url := baseUrl + endpoint
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, fmt.Errorf("Error reading request: %s", err)
	}

	req.Header.Set("Private-Token", token)
	client := &http.Client{Timeout: time.Second * 60}
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("Error reading request: %s", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Status error for %s: %s", url, resp.Status)
	}

	return resp, nil
}

func nextPage(header http.Header) bool {
	check := header.Get("Link")
	if !strings.Contains(check, "next") {
		return false
	}
	return true
}

func getNextPage(header http.Header) (string, error) {
	linkheader := header.Get("Link")
	links := strings.Split(linkheader, ",")
	for _, link := range links {
		if strings.Contains(link, "next") {
			li := strings.Split(link, ">;")[0]
			l := strings.Split(li, "v4/")[1]
			return l, nil
		}
	}
	return "", fmt.Errorf("Next page header found, but no link found.")
}

func getIds(endpoint string) ([]int, error) {
	var ids []int

	next := true
	for next {
		resp, err := getRequest(endpoint)
		if err != nil {
			return nil, fmt.Errorf("%s", err)
		}
		decoder := json.NewDecoder(resp.Body)
		var object objectIds
		derr := decoder.Decode(&object)
		if derr != nil {
			return nil, fmt.Errorf("%s", derr)
		}
		for _, i := range object {
			ids = append(ids, i.ID)
		}
		next = nextPage(resp.Header)
		if next {
			endpoint, err = getNextPage(resp.Header)
			if err != nil {
				return nil, fmt.Errorf("%s", err)
			}
		}
		resp.Body.Close()
	}
	return ids, nil
}

func getUser(uid int) (user, error) {
	endpoint := fmt.Sprintf("users/%d", uid)
	resp, err := getRequest(endpoint)
	if err != nil {
		return user{}, fmt.Errorf("error getting user %d: %s", uid, err)
	}
	defer resp.Body.Close()
	var u user
	if derr := json.NewDecoder(resp.Body).Decode(&u); derr != nil {
		return user{}, fmt.Errorf("error decoding user object: %s", derr)
	}
	return u, nil
}

func getMembers(gid int, users map[int]user) {
	// Add users of group to []users slice
	uEndpoint := fmt.Sprintf("groups/%d/members?per_page=100", gid)
	uids, err := getIds(uEndpoint)
	if err != nil {
		log.Fatalf("error getting user ids for group %d: %s", gid, err)
	}
	for _, uid := range uids {
		u, err := getUser(uid)
		if err != nil {
			log.Fatalf("error getting user %d: %s", uid, err)
		}
		users[uid] = u
	}

	// Get projects in group
	pEndpoint := fmt.Sprintf("groups/%d/projects?per_page=100", gid)
	pids, err := getIds(pEndpoint)
	if err != nil {
		log.Fatalf("error getting project ids for group %d: %s", gid, err)
	}

	if len(pids) > 0 {
		for _, pid := range pids {
			endpoint := fmt.Sprintf("projects/%d/members?per_page=100", pid)
			uids, err := getIds(endpoint)
			if err != nil {
				log.Fatalf("error getting users from project %d: %s", pid, err)
			}
			for _, uid := range uids {
				u, err := getUser(uid)
				if err != nil {
					log.Fatalf("error getting user %d: %s", uid, err)
				}
				users[uid] = u
			}
		}
	}

	// Get subgroups in group
	sgEndpoint := fmt.Sprintf("groups/%d/subgroups?per_page=100", gid)
	sgids, err := getIds(sgEndpoint)
	if err != nil {
		log.Fatalf("error getting subgroup ids for group %d: %s", gid, err)
	}

	// If there are subgroups, recurse
	if len(sgids) > 0 {
		for _, sgid := range sgids {
			getMembers(sgid, users)
		}
	}
}
